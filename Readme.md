# Projet M1-TRI de Romain Guernier & Clément Ronnelle & Louis Bouchet. 
But : Créer une petite application qui utiliser plusieurs pods qui communiquents entre eux.
Ayant déjà mis en place l'application Nextcloud avec un backen MySQL, nous nous sommes tourné vers cette application qui permet de tout tester assez rapidement une fois en place.
Ici kubectl fonctionnait sans "microk8s.", si besoin le rajouter devant les commandes.
###  D'abord on créer nos secrets pour que la base de données soit mises en places avec : 
```
kubectl create secret generic nextcloud-db-secret \
    --from-literal=MYSQL_ROOT_PASSWORD=etrs712@Soleil*/ \
    --from-literal=MYSQL_USER=nextcloud \
    --from-literal=MYSQL_PASSWORD=Soleil1234*/
```
	
### Ensuite on créer le volume persistant : 
```
kubectl apply -f nextcloud-pv-creation.yaml
```
(par défault dans /var/tmp, pour le changer : `sed -i 's#/var/tmp#{emplacement}#g' nextcloud-pv-creation.yaml` )
###  Puis  le claim du volume persistant :
```
kubectl apply -f nextcloud-shared-pvc.yaml
```
### Ensuite on déploie la base de données et son service : 
```
kubectl apply -f nextcloud-db.yaml
```
### Enfin on créer le serveur nextcloud et son service : 
```
kubectl apply -f nextcloud-server.yaml
```
### On applique le loadbalancer vers le service : 
(ne pas oublier de changer son ip externe par celle de l'hôte) 
```
microk8s.kubectl expose deployment nextcloud-server --type=LoadBalancer --name=nextcloud-ext1 --port=80 --external-ip=172.17.100.121
```

Ensuite une fois sur l'interface web pour tester la communication avec la base de données, créer un utilisateur et ensuite ouvrir l'option "storage & database"
Selectionner  "Mysql/MariaDB"
Remplir en utilisant les secrets créés auparavant, puis comme adresse "**nextcloud-db:3306**"
### Si cela ne fonctionne pas, récuperer la clusterip du service nextcloud-db, et l'utiliser à la place de "nextcloud-db". Certaines installations expérimentales ne viennent pas avec la résolution dns interne. (retour d'expérience)

Ensuite on récupere l'interface de nextcloud avec une arborescence : 

![](images/nextcloud-working.png)

Une fois cette affichage, on valide que la communications entre les pods est effective.
